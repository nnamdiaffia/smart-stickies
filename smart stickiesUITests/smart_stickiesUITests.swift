//
//  smart_stickiesUITests.swift
//  smart stickiesUITests
//
//  Created by The Grand Wizard  on 18/11/2015.
//  Copyright © 2015 The Grand Wizard . All rights reserved.
//

import XCTest

class smart_stickiesUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testTableCellData(){
        
    XCUIApplication().tables.staticTexts["i like goats"].tap()
    
    
    } // Testing the table cell data for persistence, 
    //test suceeded
    
    func testEditButton(){
        
        let app = XCUIApplication()
        let notesNavigationBar = app.navigationBars["Notes"]
        notesNavigationBar.buttons["Edit"].tap()
        
        let tablesQuery = app.tables
        let deleteILikeGoatsButton = tablesQuery.buttons["Delete i like goats"]
        deleteILikeGoatsButton.tap()
        tablesQuery.staticTexts["i like goats"].tap()
        deleteILikeGoatsButton.tap()
        app.otherElements.containingType(.NavigationBar, identifier:"Notes").childrenMatchingType(.Other).element.childrenMatchingType(.Other).element.childrenMatchingType(.Table).element.tap()
        
        let reorderILikeGoatsButton = tablesQuery.buttons["Reorder i like goats"]
        reorderILikeGoatsButton.tap()
        reorderILikeGoatsButton.tap()
        
        let reorderINeedHelpButton = tablesQuery.buttons["Reorder i need help"]
        reorderINeedHelpButton.tap()
        reorderINeedHelpButton.tap()
        notesNavigationBar.buttons["Done"].tap()
        
    } // testing edit button, 
    // test suceeded
    
    func testShareButton(){
        
        
        let app = XCUIApplication()
        let tablesQuery = app.tables
        let learnToCookStaticText = tablesQuery.staticTexts["Learn to cook"]
        learnToCookStaticText.tap()
        tablesQuery.buttons["Share"].tap()
        app.sheets.collectionViews.collectionViews.buttons["Mail"].tap()
        learnToCookStaticText.tap()
        
    } // testing share button functionality
    // test suceeded
}

