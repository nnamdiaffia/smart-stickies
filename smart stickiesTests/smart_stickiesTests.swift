//
//  smart_stickiesTests.swift
//  smart stickiesTests
//
//  Created by The Grand Wizard  on 18/11/2015.
//  Copyright © 2015 The Grand Wizard . All rights reserved.
//

import XCTest
@testable import Smart_Stickies

class smart_stickiesTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testColourPicker() {
        func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCellWithIdentifier("NoteCell", forIndexPath: indexPath)
            
            if(indexPath.row % 2 == 0)
            {
                cell.backgroundColor = UIColor.greenColor()
            }
            else if(indexPath.row % 2 == 1)
            {
                cell.backgroundColor = UIColor.yellowColor()
            }
            
            return cell
            //
        }

        // Test succeeded
    }
    
    func testPerformanceAddNewNote() {
        // This is an example of a performance test case.
        self.measureBlock {
            func addnewnote(sender: UIBarButtonItem) {
                NSLog("new note")
               
            // testing the AddNewNote button
                // test succeeded
        }
    }
    
}
    func testPerformanceNoteList(){
        
        self.measureBlock{
            
            func saveList() {
                print("list saved")

        } // Testing the save notwlist function, 
            //test suceeded
    }
}
}