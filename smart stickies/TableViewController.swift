//
//  TableViewController.swift
//  smart stickies
//
//  Created by The Grand Wizard  on 08/12/2015.
//  Copyright © 2015 The Grand Wizard . All rights reserved.
//

import UIKit

class TableViewController: UITableViewController {
    
    var note = ["Get star wars battlefront", "Learn to cook", "Read hamlet again"]
    var cell = UITableViewCell()
    
    @IBOutlet weak var addButton: UIBarButtonItem!
    @IBOutlet weak var editButton: UIBarButtonItem!
    // Navigation bar button outlets
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.leftBarButtonItem = self.editButtonItem() // exposing disclosure icon when edit button is clicked
        
        print("view did load")
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func addnewnote(sender: UIBarButtonItem) {
        NSLog("new note")
        let alert = UIAlertController(title: "New Note", message: "Enter Note Below", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addTextFieldWithConfigurationHandler(nil)
            alert.addAction(UIAlertAction(title: "Add", style: UIAlertActionStyle.Default, handler: {(action)->
            Void in
            if let note:String = alert.textFields![0].text {
                print(self.note)
                self.note.append(note)
                print(self.note)
                self.saveList()
                self.tableView.reloadData() // Refreshing the table view
            }         }))
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: nil))
            presentViewController(alert, animated: true, completion: nil)
        // Adding a new note to the list
    }
    
    func saveList() {
        let savedNotes = NSUserDefaults.standardUserDefaults()
        savedNotes.setObject(note, forKey: "notes")
        savedNotes.synchronize()
        print("list saved")
    }
    // method to save notes to list
    
    override func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? {
        /* here we define our sole share action. Notice the three parameters. The first defines the style, the second is the label that appears and the final is a completion handler that runs when the action is clicked. */
        let shareAction = UITableViewRowAction(style: .Normal, title: "Share", handler: {(action: UITableViewRowAction, indexPath: NSIndexPath) -> Void in
            let note = self.note[indexPath.row]
            /* a UIActivityViewController offers various services from the underlying system. We pass the items we want to share and the system presents suitable services based on this information. */
            let activityViewController = UIActivityViewController(activityItems: [note], applicationActivities: nil)
            /* having configured the activityViewController we need to display it. */
            self.presentViewController(activityViewController, animated: true, completion: nil)
               })
        
        shareAction.backgroundColor = UIColor.orangeColor()
        
        let deleteAction = UITableViewRowAction(style: .Normal, title: "Delete", handler: {(action: UITableViewRowAction, indexPath: NSIndexPath) -> Void in
            _ = self.note[indexPath.row]
          self.tableView.reloadData()
        })
        deleteAction.backgroundColor = UIColor.redColor()
                /* we return an array of the actions. These get displayed when we swipe a table cell left. */
        return [shareAction, deleteAction]
    }

    // MARK: - Table view data source
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return note.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("NoteCell", forIndexPath: indexPath)
        
        // Configure the cell...
        if let label:UILabel = cell.textLabel {
            label.text = self.note[indexPath.row]
            
        }
        
        
        if(indexPath.row % 2 == 0)
        {
            cell.backgroundColor = UIColor.greenColor()
        }
        else if(indexPath.row % 2 == 1)
        {
            cell.backgroundColor = UIColor.yellowColor()
        }
        
            return cell
        //
    }
    
    
                
    override func tableView(tableView: UITableView, moveRowAtIndexPath sourceIndexPath: NSIndexPath, toIndexPath destinationIndexPath: NSIndexPath) {
        let movedObject = self.note[sourceIndexPath.row]
        note.insert(movedObject, atIndex: destinationIndexPath.row)
        NSLog("it worked", "\(sourceIndexPath.row) => \(destinationIndexPath.row) \(note)")
    } // Implemented cell moving function
    
}
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.



    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

/*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }*/


    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */


