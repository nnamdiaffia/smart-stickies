//
//  colors.swift
//  smart stickies
//
//  Created by Nnamdi Okore-Affia on 11/12/2015.
//  Copyright © 2015 The Grand Wizard . All rights reserved.
//

import Foundation
import UIKit

class Colors {
    
    private var colors: [UIColor]
    
    static let sharedInstance = Colors()
    
    
    private init() {
        self.colors = []
    }
    
    
    func addColor(colors:UIColor) {
        self.colors.append(colors)
    }
    
    
    var colorPicker:[UIColor] {
        get {
            return self.colors
        }
     
    }
}