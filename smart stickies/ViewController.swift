//
//  ViewController.swift
//  smart stickies
//
//  Created by The Grand Wizard  on 18/11/2015.
//  Copyright © 2015 The Grand Wizard . All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var done: UIButton!
    @IBOutlet var firstpage: UIView!
    @IBOutlet weak var yellowButton: UIButton!
    @IBOutlet weak var redButton: UIButton!
    @IBOutlet weak var greenButton: UIButton!
    @IBOutlet weak var pinkButton: UIButton!
    @IBOutlet weak var orangeButton: UIButton!
    @IBOutlet weak var blueButton: UIButton!
    // set a color on a button
    
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        yellowButton.backgroundColor = UIColor.yellowColor()
        // Creating instances of view controller
        // Do any additional setup after loading the view, typically from a nib.
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
        

}