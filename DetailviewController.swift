//
//  DetailviewController.swift
//  smart stickies
//
//  Created by The Grand Wizard  on 18/12/2015.
//  Copyright © 2015 The Grand Wizard . All rights reserved.
//

import Foundation
import UIKit

class DetailViewController : UIViewController{
    
    @IBOutlet weak var TextView: UITextView!
    
    var TextViewText = String()
    
    
    override func viewDidLoad() {
        TextView.text = TextViewText
    }
}
